#!/bin/bash

usage() {
	echo "Converte os estudos em TXT para ADOC" >&2
	echo " Ordem: - ; + ; # ; * ; $ ; _ ; @" >&2
    echo "  Usage: $0 -i INPUT -o OUTPUT -m MATÉRIA" >&2
  exit 1
}
### Cria o caderno da matéria ##########################################
materias(){
touch /home/alysson/git/adocs/cadernos/${m}.adoc
echo "" > /home/alysson/git/adocs/cadernos/${m}.adoc
cat << EOF >> /home/alysson/git/adocs/cadernos/${m}.adoc
////
= 
:Author: Felipe_Scarel
:Author Initials: FS
////
:doctype: article
:source-highlighter: rouge
:listing-caption: Listing
:pdf-page-size: A4
:revdate: 08-12-2018
:imagesdir: ./img
:srcdir: ../src
:icons: font
:toc:
//:gabarito: true

ifdef::basebackend-html[]
++++
<link rel="stylesheet"  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css">
++++
endif::[]
include::../share/attributes.adoc[]
EOF
for file in $( ls src/*.adoc ); do
	base="$(basename $file .adoc)"
	echo -e "\ninclude::../$file[]\n\n<<<" >>  /home/alysson/git/adocs/cadernos/$m.adoc
#	echo -e " base: '$base'"
#	asciidoctor -a oneof -o html/caderno.html $file
done
asciidoctor /home/alysson/git/adocs/cadernos/$m.adoc
}

cabecalho(){
touch /home/alysson/git/adocs/src/$basename.adoc
echo "" > /home/alysson/git/adocs/src/$basename.adoc
cat << EOF >> /home/alysson/git/adocs/src/$basename.adoc
ifdef::oneof[]
////
= $basename
:Author: Alysson
:Author Initials: AR
////
:doctype: article
:source-highlighter: rouge
:listing-caption: Listing
:pdf-page-size: A4
:revdate: 12-11-2018
:imagesdir: ./img
:srcdir: ./src
:icons: font
include::../share/attributes.adoc[]
endif::oneof[]
EOF
}
## Ordem: - ; + ; # ; * ; $ ; _ ; @
txttoadoc() {
	echo -e "$basename"
	###### Tratamento do título #######################################################
	grep -e "^[#]\+" ${i} | sed ':a;$!N;s/\n//;ta;' > /tmp/$basename
	grep -v -e "^[#]\+" ${i} >> /tmp/$basename

	sed -i 's/^[\#]\+/==/g' /tmp/$basename
	sed -i 's/[#][#]\+//g' /tmp/$basename
	
	###### Tratamento do subtítulo #######################################################
	#sed -i 's/^[\+]\+/===/g' /tmp/$basename
	#sed -i ':a;$!N;s/===\n/===\ /g;ta' /tmp/$basename
	#sed -i ':a;$!N;s/[\+]\+\n\-/==/g;ta' /tmp/$basename

	sed -i -r 's/^[A-Za-z]+/\n===\ &/g' /tmp/$basename
	
	sed 's/^\-\t/\-\ /g ;
	 s/\t[\+]/\t\*/g ;
	 s/\t\t[\#]/\t\t\**/g ; 
	 s/\t\t\t[\*]/\t\t\t\***/g ;
	 s/\t\t\t\t[\$]/\t\t\t\t\****/g;
	 s/\t\t\t\t\t[\_]/\t\t\t\t\t\..../g;
	 s/\t\t\t\t\t[\@]/\t\t\t\t\t\t\...../g;
	 s/obs:/\n\TIP:/g' /tmp/$basename  >> /home/alysson/git/adocs/src/$basename.adoc
}

comp(){
	echo -e "${o}/$basename.html"
	asciidoctor -a oneof -o ${o}/$basename.html /home/alysson/git/adocs/src/$basename.adoc 
}

while getopts ":i:o:m:d" opt; do
  case "$opt" in
    i)
      i=${OPTARG}
      ;;
    o)
      o=${OPTARG}
      ;;
    m)
      m=${OPTARG}
      ;;
    *)
      usage
      ;;
  esac
done
basename="$(basename $i .txt)"
usage
echo "${o}"
cabecalho
txttoadoc
comp
materias
