
ifdef::oneof[]
////
= direito_processual_penal
:Author: Alysson
:Author Initials: AR
////
:doctype: article
:source-highlighter: rouge
:listing-caption: Listing
:pdf-page-size: A4
:revdate: 12-11-2018
:imagesdir: ./img
:srcdir: ./src
:icons: font
include::../share/attributes.adoc[]
endif::oneof[]
== Direito Processual Penal 


=== INQUÉRITO POLICIAL
- Conjunto de diligencias realizadas pela Polícia Judiciária (PF e PC) para a apuração de uma infração penal e sua autoria, a fim de que o titular da ação penal possa ingressar em juízo
	*	Polícia Militar -> Polícia ADMINISTRATIVA
		**	não investiga. Previne
- O IP é administrativo
	*	pré processual
- IP é inquisitivo
	*	não há acusação
	*	procedimento de reunir informações
	*	valor probatório pequeno
	*	ação penal pública incondicionada
		**	autoridade policial deve instaurar o IP 
		**	se o MP já dispuser dos elementos necessários ao ajuizamento da ação penal
			***	IP não precisa ser iniciado
- FORMALIDADE
	*	Todos os atos produzidos no bojo do IP deverão ser escritos
- Indisponibilidade
	*	autoridade policial não pode arquivá-lo
	*	apenas judiciário
- Dispensabilidade
	*	dispensável
	*	caso o titular da açãoo penal já possua todos os elementos necessários ao oferecimento
- Discricionariedade na sua condução
	*	sem necessidade de seguir um padrão pré-estabelecido. 
- Sigiloso
	*	sempre sigiloso em relação às pessoas do povo em geral
	*	não é sigiloso em relação aos envolvidos
	*	pode ser decretado sigilo em relação a determinadas peças
- Formas de instauração
	*	de Ofício
		**	sem necessidade de requerimento de quem quer que seja
		**	pública incondicionada
	*	Requisição do Juiz ou do MP
		**	requisição do Juiz ou do MP
		**	Delegado pode recusar caso:
			***	manifestamente ilegal
			***	Não contiver os elementos fáticos mínimos 
	*	Requerimento da vítima ou de seu representante legal
		**	Delegado não está obrigado a instaurar o IP
		**	Caso seja indeferido o requerimento, caberá recurso para o Chefe de Polícia.
		**	prazo de seis meses -> extinta a punibilidade 
	*	Auto de Prisão em Flagrante
	*	Requisição do Ministro da Justiça
		**	crimes cometidos por estrangeiro contra brasileiro fora do Brasil 
		**	crimes contra a honra cometidos contra o Presidente ou contra qualquer chefe de governo estrangeiro 
		**	...
	*	foro privilegiado depende de autorização do Tribunal
- TRAMITAÇÂO DO IP
	*	Diligências
	*	Prazo
		**	Indiciado preso: 10 dias
		**	Indiciado solto: 30 dias
		**	Crimes Federais
			***	Indiciado preso: 15 dias (+ 15 dias)
			***	Indiciado solto: 30 dias
		**	LEI DE DROGAS
			***	Indiciado preso: 30 dias (+ 1x)
			***	Indiciado solto: 90 dias (+ 1x)
		**	CRIMES CONTRA A ECONOMIA POPULAR
			***	10 dias
- redistribuído por superior hierárquico
	*	mediante despacho fundamentado
		**	por motivo de interesse público 
		**	inobservância dos procedimentos previstos em regulamento da corporação que prejudique a eficácia da investigação
- se sigiloso acesso limitado à defesa


=== Provas	
- produzido pelas partes ou mesmo pelo Juiz, visando à formação do convencimento
- diretas
	*	provam o próprio fato, de maneira direta
- indiretas
	*	não provam diretamente o fato, mas por uma dedução lógica, acabam por prová-lo.
- plenas
	*	trazem a possibilidade de um juízo de certeza quanto ao fato, possibilitando ao Juiz fundamentar sua decisão de mérito em apenas uma delas
- não-plenas
	*	Apenas ajudam a reforçar a convicção do Juiz, não pode fundamentar sua decisão de mérito apenas numa prova não-plena.
- reais
	*	se baseiam em algum objeto, e não derivam de uma pessoa.
- pessoais
	*	derivam de uma pessoa.
- típica
	*	Seu procedimento está previsto na Lei.
- atípica
	*	não está prevista na Legislação (este conceito se confunde com o de prova, inominada); 
	*	está prevista na Lei, mas seu procedimento não, quanto aquela em que nem ela nem seu procedimento estão previstos na Legislação
- anômala
	*	prova típica, utilizada para fim diverso daquele para o qual foi originalmente prevista
- irritual
	*	há procedimento previsto na Lei, só que este procedimento não é respeitado quando da colheita da prova.
	"fora da terra"
	*	realizada perante juízo distinto daquele perante o qual tramita o processo
- crítica
	*	sinônimo de "prova pericial".
- Sistema quanto à apreciação:
	*	do livre convecimento motivado da prova
		**	Juiz deve valorar a prova produzida da maneira que entender mais conveniente
- Etapas:
	*	Proposição
		**	produção da prova é requerida ao Juiz
	*	Admissão
		**	o Juiz defere ou não a produção de uma prova.
	*	Produção
		**	a prova é trazida para dentro do processo.
	*	Valoração
		**	o Juiz aprecia cada prova produzida e lhe atribui o valor
- Provas ilegais
	*	Provas ilícitas
		**	produzidas mediante violação de normas de direito MATERIAL
			***	Ex.: Prova obtida mediante tortura.
	*	Provas ilícitas por derivação
		**	embora sejam lícitas em sua essência, DERIVAM de uma prova ILÍCITA"
			***	Ex.: obtida mediante depoimento válido. Mas só se descobriu a testemunha em razão de uma interceptação telefônica ilegal.
		**	pode ser usada:
			***	Não havia nexo de causalidade entre a prova ilícita e a prova derivada
			***	a derivada poderia ter sido obtida por fonte independente ou seria, inevitavelmente, descoberta pela autoridade
	*	Provas ilegítimas
		** 	obtidas mediante VIOLAÇÃO à normas de caráter PROCESSUAL, sem que haja violação a normas constitucionais
- EXAME DE CORPO DE DELITO E PERÍCIAS EM GERAL
	*	Direto
		**	realizado diretamente sobre o vestígio
	*	Indireto
		**	baseado em informações verossímeis fornecidas a ele
	*	Pode ocorrer:
		**	na fase investigatória
		**	na fase de instrução do processo criminal
	*	obrigatório nos crimes que deixam vestígios
		**	nulidade do processo caso contrário
	*	dispensado no caso de infrações de menor potencial ofensivo
		**	desde que a inicial acusatória esteja acompanhada de boletim médico, equivalente
	*	Deve ser realizado por 01 perito oficial
	*	As partes, o ofendido e o assistente de acusação podem formular quesitos, indicar assistentes técnicos e requerer esclarecimentos aos peritos
		**	atuará a partir de sua admissão pelo juiz e após a conclusão dos exames e elaboração do laudo pelos peritos oficiais
		**	fase judicial
	*	Divergência entre os peritos
		**	Cada um elaborará seu laudo
		**	autoridade deverá nomear um 3º perito
		**	case 3º perito discorde de ambos, a autoridade poderá mandar proceder à realização de um novo exame pericial
	*	Juiz pode discordar do laudo
	*	Na falta de perito oficial:
		**	o exame será realizado por 2 pessoas idôneas
		**	 curso superior preferencialmente na área específica
- INTERROGATÓRIO DO RÉU
	*	meio de prova e meio de defesa do réu
	*	obrigatoriamente na presença de seu advogado
	*	silêncio não importa confissão e não pode ser interpretado em prejuízo da defesa
	*	1º	perguntas sobre sua pessoa
	*	2º	perguntas acerca do fato
- CONFISSÃO
	*	Verossimilhança das alegações do réu aos fatos,
	*	A clareza do réu na exposição dos motivos,
	*	Coincidência com o que apontam os demais meios de prova
	*	Pessoalidade
		**	Não se pode ser feita por procurador
	*	Caráter expresso
		**	Não se admite confissão tácita no Processo Penal, devendo ser manifestada e reduzida a termo
	*	Oferecimento perante o Juiz COMPETENTE
	*	Espontaneidade
		**	Não pode ser realizada sob coação
	*	Capacidade do acusado para confessar 
	*	Não possui valor absoluto
	*	retratável
	*	divisivel
- OITIVA DO OFENDIDO
	*	ofendido NAO É TESTEMUNHA
		**	NÃO responde por falso testemunho
- PROVA TESTEMUNHAL
	*	escritos, instrumentos ou papéis, públicos ou particulares
	*	Pessoas dispensadas de prestar compromisso
		**	Doentes e deficientes mentais
		**	Menores de 14 anos
		**	Ascendente ou descendente, o afim em linha reta, o cônjuge, ainda que desquitado, o irmão e o pai, a mãe, ou o filho adotivo do acusado
	*	Contradita
		**	impugnação à testemunha
			***	Pessoas que não devam prestar compromisso
				****	Arrolada por qualquer das partes
				****	tomada do seu depoimento sem compromisso legal
			***	NÃO PODEM DEPOR
				****	terem tomado ciência do fato em razão do ofício ou profissão
				****	Contraditadas, devem, ser EXCLUIDAS
- RECONHECIMENTO DE PESSOAS E COISAS
	*	deverá descrever a pessoa que deva ser reconhecida
	*	A pessoa será colocada, se possível, ao lado de outras que com ela tiverem qualquer semelhança
	*	não é obrigatória a presença de advogado ba delegacia
		**	reconhecimento pode ser refeito na etapa judicial
- ACAREAÇÃO
	*	duas pessoas, que prestaram informações divergentes, são colocadas "frente a frente"
	*	testemunhas, acusados e ofendidos
	*	também pode ser feita mediante carta precatória
- INDÍCIOS
	*	NÃO PROVAM o fato que se discute
	*	provam outro fato, a ele relacionado, que faz INDUZIR que o fato discutido ocorreu ou não
- BUSCA E APREENSÃO
	*	pode ocorrer na fase judicial ou na fase de investigação policial
	*	só poderá ser realizada durante o dia
	*	se não houver ninguém em casa? 
		**	CPP determina que seja intimado algum vizinho para que presencie o ato
	

=== Prisão em flagrante	
- natureza administrativa
	*	não depende de autorização judicial
- Flagrante próprio
	*	indivíduo que está cometendo o fato criminoso
	*	ou acaba de cometer este fato
	*	flagrante real, verdadeiro ou propriamente dito.
- Flagrante impróprio
	*	é necessário que haja uma perseguição ou busca pelo indivíduo, 
	*	imperfeito, irreal ou "quase flagrante".
- Flagrante presumido 
	* 	mesmas características do flagrante impróprio
	*	não exige que tenha havida qualquer perseguição ao suposto infrator, 
	*	surpreendido, logo depois do crime, com objetos (armas, papéis, etc .... ) que façam presumir que ele foi o autor do delito. 
	*	flagrante ficto ou assimilado
- Apresentação expontânea
	*	não há flagrante
- Crimes permanentes
	*	O flagrante pode ser realizado em qualquer momento durante a execução do crime, logo após ou logo depois.
- Crimes continuados
	*	Por se tratar de um conjunto de crimes que são tratados como um só para efeito de aplicação da pena, pode haver flagrante quando da ocorrência de qualquer dos delitos
- flagrante preparado 
	
